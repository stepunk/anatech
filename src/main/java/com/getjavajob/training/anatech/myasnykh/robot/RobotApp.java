package com.getjavajob.training.anatech.myasnykh.robot;

import com.getjavajob.training.anatech.myasnykh.robot.robots.Robot;
import com.getjavajob.training.anatech.myasnykh.robot.robots.RobotImpl;

public class RobotApp {
    public static void main(String[] args) {
        Robot robot = new RobotImpl();
        System.out.println("Total robot's weight: " + robot.getWeight());
        robot.printRobotParts();
        robot.printRecursiveDivision();
    }
}
