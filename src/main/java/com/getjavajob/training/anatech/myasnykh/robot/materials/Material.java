package com.getjavajob.training.anatech.myasnykh.robot.materials;

public enum Material {
    STEEL("Steel", 10, 20), CAST_IRON("Cast iron", 21, 30), PLASTIC("Plastic", 1, 9);
    private String name;
    private int min;
    private int max;

    Material(String name, int min, int max) {
        this.name = name;
        this.min = min;
        this.max = max;
    }

    public String getName() {
        return name;
    }

    public int getMinWeight() {
        return min;
    }

    public int getMaxWeight() {
        return max;
    }
}
