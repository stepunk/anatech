package com.getjavajob.training.anatech.myasnykh.robot.parts;

import com.getjavajob.training.anatech.myasnykh.robot.materials.Material;

public abstract class AbstractPart implements Part {
    private final String partName;
    private final int weight;
    private final Material material;

    public AbstractPart(String partName, Material material) {
        this.partName = partName;
        this.material = material;
        this.weight = getRandomWeight(material);
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String getMaterialName() {
        return material.getName();
    }

    @Override
    public String getPartName() {
        return partName;
    }

    protected int getRandomWeight(Material material) {
        int min = material.getMinWeight();
        int max = material.getMaxWeight();
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}
