package com.getjavajob.training.anatech.myasnykh.robot.parts;

import com.getjavajob.training.anatech.myasnykh.robot.materials.Material;

public class Arms extends AbstractPart {
    public Arms(Material material) {
        super("Arms", material);
    }
}
