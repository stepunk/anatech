package com.getjavajob.training.anatech.myasnykh.robot.parts;

import com.getjavajob.training.anatech.myasnykh.robot.materials.Material;

public class Body extends AbstractPart {
    public Body(Material material) {
        super("Body", material);
    }
}
