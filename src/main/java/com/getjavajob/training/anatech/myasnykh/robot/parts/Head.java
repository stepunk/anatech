package com.getjavajob.training.anatech.myasnykh.robot.parts;

import com.getjavajob.training.anatech.myasnykh.robot.materials.Material;

public class Head extends AbstractPart {
    public Head(Material material) {
        super("Head", material);
    }
}
