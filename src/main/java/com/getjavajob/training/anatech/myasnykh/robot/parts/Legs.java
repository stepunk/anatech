package com.getjavajob.training.anatech.myasnykh.robot.parts;

import com.getjavajob.training.anatech.myasnykh.robot.materials.Material;

public class Legs extends AbstractPart {
    public Legs(Material material) {
        super("Legs", material);
    }
}
