package com.getjavajob.training.anatech.myasnykh.robot.parts;

public interface Part {
    int getWeight();

    String getMaterialName();

    String getPartName();
}
