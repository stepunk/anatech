package com.getjavajob.training.anatech.myasnykh.robot.robots;

public interface Robot {
    int getWeight();

    void printRobotParts();

    void printRecursiveDivision();
}
