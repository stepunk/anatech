package com.getjavajob.training.anatech.myasnykh.robot.robots;

import com.getjavajob.training.anatech.myasnykh.robot.materials.Material;
import com.getjavajob.training.anatech.myasnykh.robot.parts.*;

import java.util.ArrayList;
import java.util.List;

public class RobotImpl implements Robot {
    private List<Part> robotParts;

    public RobotImpl() {
        robotParts = new ArrayList<>();
        robotParts.add(new Head(getRandomMaterial()));
        robotParts.add(new Body(getRandomMaterial()));
        robotParts.add(new Arms(getRandomMaterial()));
        robotParts.add(new Legs(getRandomMaterial()));
    }

    @Override
    public int getWeight() {
        int totalSum = 0;
        for (Part part : robotParts) {
            totalSum += part.getWeight();
        }
        return totalSum;
    }

    @Override
    public void printRobotParts() {
        System.out.println("Parts of Robot:");
        for (Part part : robotParts) {
            System.out.println(part.getPartName());
            System.out.println("Material: " + part.getMaterialName());
            System.out.println("Weight: " + part.getWeight());
            System.out.println();
        }
        System.out.println("Total weight:" + getWeight() + "\n");
    }

    /**
     * method takes value of the robot's weight and recursively divides it and prints result, the robot's weight remains unchanged
     */
    @Override
    public void printRecursiveDivision() {
        System.out.println("Recursive division:");
        divide(getWeight(), 2);
    }

    private void divide(int number, int divider) {
        if (number > 0) {
            int result = number / divider;
            System.out.println(result);
            divide(result, divider);
        }
    }

    private Material getRandomMaterial() {
        Material[] materials = Material.values();
        int index = (int) (Math.random() * materials.length);
        return materials[index];
    }
}
